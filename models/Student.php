<?php

namespace app\models;

use yii\db\ActiveRecord;

class Student extends ActiveRecord 
{

	public static function tableName() 
	{
		return 'student';
	}

	public static function getName($id)//static because we dont create an object
	{
		$student = self::findOne($id);
		
		isset($student)? //use if statement
		$return = $student->name: //if true
		$return = "No student found with id $id"; //if false
		return $return;
	}
	

}
