<?php

use yii\db\Migration;

class m170517_192039_init_table_customers extends Migration
{
    public function up()
    {
 
        $this->addColumn('customers','description',  $this->text());
    
    }

   /* public function down()
    {
		  $this->dropColumn('customers','description');
        echo "m170517_192039_init_table_customers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
