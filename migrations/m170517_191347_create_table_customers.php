<?php

use yii\db\Migration;

class m170517_191347_create_table_customers extends Migration
{
    public function up()
    {
$this->createTable(
'customers',
[
'id'=>'pk',
'name'=>'string',
'email'=>'string',

],
'ENGINE=InnoDB'


);
    }

    public function down()
    {
		$this->dropTable('customers');
        echo "m170517_191347_create_table_customers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
